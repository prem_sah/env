git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle
sudo apt-get install tmux
# From https://github.com/Valloric/YouCompleteMe/wiki/Building-Vim-from-source
sudo apt-get install libncurses5-dev libgnome2-dev libgnomeui-dev \
libgtk2.0-dev libatk1.0-dev libbonoboui2-dev \
libcairo2-dev libx11-dev libxpm-dev libxt-dev python-dev ruby-dev mercurial
sudo apt-get remove vim vim-runtime gvim
sudo apt-get remove vim-tiny vim-common vim-gui-common
cd ~
hg clone https://code.google.com/p/vim/
cd vim
./configure --with-features=huge \
            --enable-rubyinterp \
            --enable-pythoninterp \
            --with-python-config-dir=/usr/lib/python2.6-config \
            --enable-perlinterp \
            --enable-gui=gtk2 --enable-cscope --prefix=/usr --enable-luainterp
make VIMRUNTIMEDIR=/usr/share/vim/vim74
sudo apt-get install checkinstall
sudo checkinstall
sudo update-alternatives --install /usr/bin/editor editor /usr/bin/vim 1
sudo update-alternatives --set editor /usr/bin/vim
sudo update-alternatives --install /usr/bin/vi vi /usr/bin/vim 1
sudo update-alternatives --set vi /usr/bin/vim
sudo apt-get install cmake # Needed for YouCompleteME
